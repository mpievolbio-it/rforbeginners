[R basic plotting <<<](RBASICPLOTTING.html)

## Control structures

R has the standard **control structures** you would expect.

The **expression** can be multiple (compound) statements by enclosing them in braces { }.

{% hint style='tip' %}
In most cases it is more efficient to use **built-in** functions rather than **control structures** whenever possible.
{% endhint %}

### if and else

```r
## if (cond) {expr}
if (3 > 2) {cat("3 is greater than 2")}
```

Here, we will write a small function that checks if a **numeric** value is greater 2.

```r
## if (cond) {expr1} else {expr2}
IfElseGeater2 <- function(x){
    ## add stop function to check if x is numeric
    if(!is.numeric(x)){
        stop("x needs to be numer")
    }
    if(x > 2){
        cat(paste0(x, " is greater than 2"))
    } else {
        cat(paste0(x, " is not greater than 2"))
    }
}
```

### for loops

For loops 

```r
## for (var in seq) {expr}
for(i in seq(from=1, to = 10)){
    cat(i, "\n")
}
```

### nested for loops

Nested for loops are a common task, e.g. first iterate over columns and second over rows.

```r
## Create a matrix with value from 1 to 100 with 10 columns and 10 rows
m <- matrix(1:100, ncol=10, nrow=10)
## first iterate over columns from 1 to 10 using i as the column index
for(i in seq(from=1, to=ncol(m))){
    ## second iterate over rows from 1 to 10 using j as the row index
    for(j in seq(from=1, to=nrow(m))){
        ## return the value
        cat("column: ", i, "row: ", j, "has the value: ",m[i, j], "\n")
    }
}
```

> #### Note::Exercise 020
> Alter the code above so that only the `upper-triangle` of the matrix is reported back.
> 
> This would correspond to an analysis where data is symetric, meaning `m[i, j]` equal `m[j, i]`.
> ```r
> ## Create a matrix with value from 1 to 100 with 10 columns and 10 rows
> m <- matrix(1:100, ncol=10, nrow=10)
> ## first iterate over columns from 1 to 10 using i as the column index
> for(i in seq(from=1, to=ncol(m))){
>     ## second iterate over rows from 1 to 10 using j as the row index
>     for(j in seq(from=1, to=nrow(m))){
>         ## return the value
>         cat("column: ", i, "row: ", j, "has the value: ",m[i, j], "\n")
>     }
> }
> ```

{% hint style='tip' %}
In R the `upper.tri()` and the `lower.tri()` function can be used to get the corresponding **logicals** of a matrix, which further can be used to extract the values. One needs to be careful of the order of the returned values.
```r
## Create a matrix with value from 1 to 100 with 10 columns and 10 rows
m <- matrix(1:100, ncol=10, nrow=10)
m[upper.tri(m)]
```
{% endhint %}

### while loops

```r
## while (cond) expr
cur <- 5
i <- 0
while(i < 5){
    cat("current i: ", i, "\n")
    i <- i +1
    cat("next i: ", i, "\n")
}
```

### next and break

`next()` is used to skip an iteration of a loop.

```r
for(i in 1:100) {
        if(i <= 20) {
                ## Skip the first 20 iterations
                next                 
        }
        ## Do something here
}
```

`break()` is used to exit a loop immediately, regardless of what iteration the loop may be on.

```r
for(i in 1:100) {
      print(i)

      if(i > 20) {
              ## Stop loop after 20 iterations
              break  
      }		
}
```

[Next >>> Loop Functions on list(), matrix() and data.frame()](LOOPFUNCTIONS.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
