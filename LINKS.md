[Solutions <<<](SOLUTIONS.html)

## Links

[https://www.rstudio.com/resources/cheatsheets/](https://www.rstudio.com/resources/cheatsheets/)

[https://www.statmethods.net/](https://www.statmethods.net/)

[https://kbroman.org/knitr_knutshell/](https://kbroman.org/knitr_knutshell/)

[https://rstudio.github.io/DT/](https://rstudio.github.io/DT/)

[https://dtkaplan.github.io/SM2-bookdown/](https://dtkaplan.github.io/SM2-bookdown/)

[https://cran.r-project.org/doc/manuals/R-intro.pdf](https://cran.r-project.org/doc/manuals/R-intro.pdf)

[Next >>> References](SOURCES.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
