[Regular expressions and strings <<<](REGEXP.html)

## tidyverse

### Why to switch from basic R usage to tidyverse?

[tidyverse](https://www.tidyverse.org/) is a set of packages designed for data science.

![tidyverse](images/tidyverse.png)

All included packages use the same:

- philosophie
- grammar
- data structure

By this a scientist should be able to work efficiently with data in R.

The packages deal with:

- data import
- data cleaning
- data transformation
- data visualization

`tidyverse` is more intuitive programming:

- the names of functions speak for themselves.
- the code is easier to read than with R base
- code is shared among tidyverse packages
- the functions are quite quick (coded more efficiently)

Here you can see some comparison of R **syntax** used by different packages.

![Rsyntax_cheat_sheet1](images/Rsyntax_cheat_sheet1.png)

![Rsyntax_cheat_sheet2](images/Rsyntax_cheat_sheet2.png)

### tidyverse core packages

The following 8 packages are included in the core tidyverse:

Data Import and Management:

- `tibble`
- `readr`

Data Wrangling (Tyding and Transformation):

- `dplyr`
- `tidyr`
- `stringr`
- `forcats`

Data Visualization and Exploration:
- `ggplot2`

Functional Programming:
- `purrr`

### Install tidyverse packages

```r
install.packages("tidyverse")
```

Load all:

```r
library(tidyverse)
```

### What are tibbles?

Modern re-thinking of data frame.

They leave behind some (sometimes user-unfriendly) features of classical data frames:
        
- No string to factor conversion
- No automatic creation of row names.
- Column names can be non-valid R variable names (for example, they can start with numbers).

#### Creating tibbles

You can create a simple `tibble` with the `tibble()` function, similarly as when you create a **data frame**:

```r
mytibble <- tibble(
  letters = letters,
  numbers = 1:26)
```

Convert a **data frame** to a tibble:

```r
myDf <- data.frame(
    letters = letters,
    numbers = 1:26)
as_tibble(myDf)
```

Convert a **tibble** to a **data frame** (useful to interact with R code that doesn’t support tibbles):

```r
as.data.frame(mytibble)
```

Selectin a columns works like for **matrix** and **data frame**.

```r
mytibble$letters
mytibble[["letters"]]
mytibble["letters"]
```

### Data import and export with readr

![data-import1](images/data-import1.png)

![data-import2](images/data-import2.png)

As the base function of R `read.table()` with `readr` there are specific function directly specifying the column **delimiter** of a table file.

The `read_tsv()` function uses **tab** whereas the `read_csv()` function uses **comma** and the `read_csv2()` function uses a **semicolon** by deafult.

If you wnat to set the delimter yourself you can use the `read_delim()` function.

```r
data(iris)
write.table(iris, file="iris.tsv", sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
myIris <- read_tsv("iris.tsv")
##
data(airquality)
write.table(airquality, file="airquality.tsv", sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
myAirquality <- read_tsv("airquality.tsv")
```

As the base function of R `write.table()` with `readr` there are cooresponding wriet functions `write_delim()`, `write_tsv()`, `write_csv()` and `write_csv2()`.

```r
?write_delim
```

### Tidy data with tidyr

`tidy` data is where:

- Each **column** describes a **variable**.
- Each **row** describes an **observation**.
- Each **value** is a **cell**.

Example of a tidy data:

| day | month | year | weight | height |
|:---:|:-----:|:----:|:------:|:------:|
| 12 | 4 | 2020 | 3.5 | 48 |
| 23 | 8 | 2019 | 2.9 | 50 |
| 9 | 11 | 2020 | 3.8 | 50 |

Example of untidy data:

| day | month,year | weight | height |
|:---:|:----------:|:------:|:------:|
| 12 | 4,2020 | 3.5 | 48 |
| 23 | 8,2019 | 2.9 | 50 |
| 9 | 11,2020 | 3.8 | 50 |

#### Seperate

To tidy your data there are specific functions like the `separate()` function.

The `separate()` function separates a column into 2 or more columns, given a specified field separator.

**table5** displays the number of tuberculosis cases documented by the WHO in selected countries:

```r
## Load example data - table5
data(table5)
table5
str(table5)
## separate column "rate" into 2 columns that we name cases and population, based on the "/" separator.
table5_1 <- separate(
    data=table5, 
    col=rate, 
    into=c("cases", "population"),
    sep="/")
table5_1
```

#### Unite

The `unite()` function unite/stick together 2 or (more) columns.

```r
## unite columns "century" and "year"
table5a_2 <- unite(
    data=table5_1,
    col=year,
    c("century", "year"), 
    sep="")
table5a_2
```

### pivoting data

### pipe operator

The **pipe** allows to process the output of a function as the input of the following function:

- it makes the code easier to read and understand

It pipes the output of a function as the first argument of the next function (from left to right):

- `mytibble %>% function1` is equivalent to `function1(mytibble)`
- `mytibble %>% function2(y)` is equivalent to `function1(mytibble, y)`
- `mytibble %>% function1 %>% function2` is equivalent to `function2(function1(mytibble))`

### Data manipulation with dplyr

The `dplyr` package was developed by Hadley Wickham of RStudio and is an optimized and distilled version of his `plyr` package. The `dplyr` package does not provide any "new" functionality to R per se, in the sense that everything `dplyr` does could already be done with base R, but it greatly simplifies existing functionality in R.

#### Grammar of dplyr 

Some of the key functions provided by the `dplyr` package are:

- `%>%` the "pipe" operator is used to connect multiple function actions together into a pipeline

- `select()` return a subset of the columns of a **data frame**, using a flexible notation

- `filter()` extract a subset of rows from a **data frame** based on logical conditions

- `arrange()` reorder rows of a **data frame**

- `rename()` rename variables in a data frame

- `mutate()` add new variables/columns or transform existing variables

- `summarise()` / `summarize()` generate summary statistics of different variables in the **data frame**, possibly within strata

The `dplyr` package as a number of its own data types that it takes advantage of. For example, there is a handy print method that prevents you from printing a lot of data to the console. Most of the time, these additional data types are transparent to the user and do not need to be worried about.

The `dplyr` function all have similar parameter:

- The first argument is a **data frame**.

- The subsequent arguments describe what to do with the data frame specified in the first argument, and you can refer to columns in the **data frame** directly without using the `$` operator (just use the column names).

- The return result of a function is a new **data frame**.

- **Data frames** must be properly formatted and annotated for this to all be useful.

In particular, the data must be `tidy`. In short, there should be **one observation per row**, and **each column** should represent a feature or characteristic of that observation.

#### select

```r
## Load example data iris
data(iris)
str(iris)
## Select by column names
iris %>% select(Sepal.Width, Species)
## Use column names 1 and 5 to select
iris %>% select(names(iris)[c(1, 5)])
```

In a similar way certain columns can be removed by adding `-`.

```r
## Load example data iris
data(iris)
str(iris)
iris %>% select(-Sepal.Width, -Species)
```

#### filter

The `filter()` function is used to extract subsets of rows from a **data frame**.

This function is similar to the existing `subset()` function in R.

```r
## Load example data iris
data(iris)
str(iris)
iris %>% filter(Sepal.Width >= 3)
## Use multiple conditions
iris %>% filter(Sepal.Width >= 3, Sepal.Length < 6)
```

#### arrange

The `arrange()` function is used to reorder rows of a **data frame** according to one of the variables/columns.

Reordering rows of a **data frame** (while preserving corresponding order of other columns) is normally a pain to do in R.

The `arrange()` function simplifies the process quite a bit.

#### rename

Renaming a variable in a **data frame** in R is surprisingly hard to do.

The `rename()` function is designed to make this process easier.

```r
data(iris)
iris %>% rename(SepalWidth = Sepal.Width)
```

#### mutate

#### summarise

#### group_by

The `group_by()` function is used to generate summary statistics from the data frame within strata defined by a variable.

#### pull

#### slice

### Data joining

The R `base::merge()` function can perform all four types of mutating join:

|dplyr|merge|
|:---:|:---:|
| `inner_join(x, y)` | `merge(x, y)` |
| `left_join(x, y)` | `merge(x, y, all.x = TRUE)` |
| `right_join(x, y)` | `merge(x, y, all.y = TRUE)` |
| `full_join(x, y)` | `merge(x, y, all.x = TRUE, all.y = TRUE)` |

{% hint style='info' %}
There is a great tutorial which might be of interest, which does not make sense to copy paste here.

Please visit the following web page to look into it and learn everything about relational data.

[https://r4ds.had.co.nz/relational-data.html](https://r4ds.had.co.nz/relational-data.html)
{% endhint %}

### String manipulation with stringr

Like the base R `strsplit()`, `substr()`, `sub()` and `gsub()` functions, the `stringr` packages provides a `tidy` way of string manipulation functions.

All functions in stringr start with `str_` and take a vector of strings as the first argument.

The `str_detect()` detects the presence or absence of a pattern in a string.

```r
examplestring <- c("genomics", "proteomics", "proteome", "transcriptomics", "metagenomics", "metabolomics")
str_detect(examplestring, pattern="genom")
```

You can use regular expressions:

As a simple example, here we want to detect which element of examplestring starts with genom.

```r
str_detect(examplestring, pattern="^genom")
```

You can reverse the search and output elements where the pattern is NOT found with the parameter `negate=TRUE`.

```r
str_detect(examplestring, pattern="^genom", negate=TRUE)
```

The `str_length()` function outputs length of strings (number of characters) in each element of a vector.

```r
str_length(examplestring)
```

The `str_replace()` function looks for a pattern in a string and replace it.

```r
str_replace(examplestring,  pattern="omics",  replacement="ome")
```

It can also be used to remove selected patterns from strings.

```r
str_replace(examplestring,  pattern="omics",  "")
```

Or one can directly use the `str_remove()` function.

```r
## str_remove is a wrapper for the same thing (no need for the 3rd argument)
str_remove(examplestring, pattern="omics")
```

You can use it inside another `tidyverse` function.

```r
data(iris)
iris %>% mutate(species2=str_replace(Species, "s", "t"))
```

### Exercises

[Next >>> ggplots](GGPLOT2.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
