[Links <<<](LINKS.html)

Mayn thanks to all people which created the external content and resources.

Without them, it would have not been possible to create this **R for Beginners** tutorial.

## References

[Havard Chan Bioinformatics Core (HBC)](https://bioinformatics.sph.harvard.edu/) [https://github.com/hbctraining/reproducibility-tools](https://github.com/hbctraining/reproducibility-tools)

[Havard Chan Bioinformatics Core (HBC)](https://bioinformatics.sph.harvard.edu/) [https://github.com/hbctraining/Intro-to-R-flipped](https://github.com/hbctraining/Intro-to-R-flipped)

Roger D. Peng. "R Programming for Data Science (2020)". [https://bookdown.org/rdpeng/rprogdatascience/](https://bookdown.org/rdpeng/rprogdatascience/)

Sarah Bonnin. "Introduction to R (2021)". [https://biocorecrg.github.io/CRG_RIntroduction_2021/](https://biocorecrg.github.io/CRG_RIntroduction_2021/)

Sarah Bonnin. "Intermediate R: introduction to data wrangling with the Tidyverse (2021)" [https://biocorecrg.github.io/CRG_R_tidyverse_2021/](https://biocorecrg.github.io/CRG_R_tidyverse_2021/)

Hadley Wickham, Mine Çetinkaya-Rundel, and Garrett Grolemund. "R for Data Science (2e)". [https://r4ds.had.co.nz/index.html](https://r4ds.had.co.nz/index.html)

Zuur, Alain F., Elena N. Ieno, and Erik HWG Meesters. "A Beginner's Guide to R." New York: Springer, 2009. [https://link.springer.com/content/pdf/10.1007%2F978-0-387-93837-0.pdf](https://link.springer.com/content/pdf/10.1007%2F978-0-387-93837-0.pdf)

Kronthaler, Franz, and Silke Zöllner. "Data Analysis with RStudio." Springer: Berlin/Heidelberg, Germany, 2021. [https://link.springer.com/content/pdf/10.1007%2F978-3-662-62518-7.pdf](https://link.springer.com/content/pdf/10.1007%2F978-3-662-62518-7.pdf)

Wickham, Hadley. "ggplot2: elegant graphics for data analysis." springer, 2016. [https://link.springer.com/content/pdf/10.1007%2F978-3-319-24277-4.pdf](https://link.springer.com/content/pdf/10.1007%2F978-3-319-24277-4.pdf)

[https://www.codecademy.com/learn/learn-r/modules/ggplot2-data-visualization-with-r](https://www.codecademy.com/learn/learn-r/modules/ggplot2-data-visualization-with-r)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
