[What is R <<<](R.html)

## RStudio

![RStudioLogo](images/RStudioLogo.png)

[RStudio](https://www.rstudio.com/) is an integrated development environment (IDE) for R and Python, with a console, syntax-highlighting editor that supports direct code execution, and tools for plotting, history, debugging and workspace management.

### RStudio Desktop installation

RStudio is available for Linux, macOS and Windows and is free for personal usage.

[https://www.rstudio.com/products/rstudio/download/#download](https://www.rstudio.com/products/rstudio/download/#download)

### RStudio Server access - GWDG

[https://rstudio.gwdg.de](https://rstudio.gwdg.de)

### RStudio Server access - MPI PLOEN (ONLY ACCESSIBLE VIA VPN CONNECTION)

[http://rstudio.evolbio.mpg.de:8787](http://rstudio.evolbio.mpg.de:8787)

### RStudio interface

- top-left: scripts and files
- bottom-left: R console
- top-right: objects, history and environment
- bottom-right: tree of folders, graph window, packages, help window, viewer

![RStudioAppearnce](images/RStudioAppearence.png)

### RStudio interactive console

![RStudioConsole](images/RStudioConsole.png)

### RStudio environment

![RStudioEnvironment](images/RStudioEnvironment.png)

### RStudio Files window

![RStudioFiles](images/RStudioFiles.png)

{% hint style='info' %}
RStudio provide some useful Cheatsheets also describing RStudio's IDE [https://www.rstudio.com/resources/cheatsheets/](https://www.rstudio.com/resources/cheatsheets/).
{% endhint %}

![RStudioIDE1](images/rstudio-ide_cheat_sheet1.png)

### R scripts

{% hint style='info' %}
Any commands that you write in the R console can be saved in to a file to be re-run again.
Files containg R code to be ran in this way are called R scripts.
R scripts have .R at the end of their names to let you know what they are.
{% endhint %}

Store commands in a .R/.r file/script.

Create and save a script in RStudio with:

- File >>> New File >>> R Script
- File opens in the upper-left panel.
- Once the file has opened: File >>> Save
- Specify a name

{% hint style='info' %}
The extension .R is automatically added.
{% endhint %}

Start writing in a .R file and use RStudio’s short cut keys for the Run command to push the current line, selected lines or modified lines to the interactive R console.

You will be able to run the file you create from within RStudio or using R’s `source()` function.

{% hint style='tip' %}
RStudio offers you great flexibility in running code from within the editor window. There are buttons, menu choices, and keyboard shortcuts. To run the current line, you can:

- click on the Run button above the editor panel, or
- select "Run Lines" from the "Code" menu, or
- hit "Ctrl+Return" in Windows or Linux or "⌘+Return" on OS X. (This shortcut can also be seen by hovering the mouse over the button).

To run a block of code, select it and then Run.

If you have modified a line of code within a block of code you have just run, there is no need to reselect the section and Run, you can use the next button along, Re-run the previous region.

This will run the previous code block including the modifications you have made.
{% endhint %}

{% hint style='tip' %}
Later in this tutorial you are going to use Rmarkdown files to create individual code blocks to run your R code.
{% endhint %}

### R working directory

One can either set the **working directory** via the **console** and the `setwd()` function.

Or one can use the RStudio`s functionality within the **Files** window.

Please change in a path of your choice and create a folder with the **New Folder** button.

![RStudioFilesWindow](images/RStudioFilesWindow.png)

Change your location in this new folder and set it as the **working directory**.

![RStudioSetwd](images/RStudioSetwd.png)

In the **console** one can check the **working directory** with the `getwd()` function.

```r
getwd()
```

### R markdown

During this R tutorial you will use the **Markdown** language to write and fill up your own documentation of this R course.

Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents.

For more details on using R Markdown see [http://rmarkdown.rstudio.com](http://rmarkdown.rstudio.com).

When you click the Knit button a document will be generated that includes both, content as well as the output of any embedded R code chunks within the document.

Please create a new **R Markdown** file with the **R Markdown** button and name it accordingly, e.g. "R_for_Beginners"

Create and save a **R Markdown** file in RStudio with:
- File >>> New File >>> R Markdown
- Specify a Title
- Choose Format HTML
- File opens in the upper-left panel.
- Once the file has opened: File >>> Save
- Specify a name

![RStudioNewMarkdown1](images/RStudioNewMarkdown1.png)

![RStudioNewMarkdown2](images/RStudioNewMarkdown2.png)

There are differnet R Markdown Types that one can choose to create.

This R course will use a simple HTML Document.

However, once you have time you should check out the other useful examples and templates provided by RStudio.

[Next >>> What is Rmarkdown](RMARKDOWN.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
