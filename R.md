[Content <<<](CONTENT.html)

## R

![Rlogo](images/Rlogo.png)

[R](https://cran.r-project.org/) is ‘GNU S’, a freely available language and environment for statistical computing and graphics which provides a wide variety of statistical and graphical techniques: linear and nonlinear modelling, statistical tests, time series analysis, classification, clustering, etc. Please consult the [R project homepage](https://cran.r-project.org/) for further information.

![R showcases](images/Rshowcases.png)

One of R’s strengths is the ease with which well-designed publication-quality plots can be produced, including mathematical symbols and formulae where needed. Great care has been taken over the defaults for the minor design choices in graphics, but the user retains full control.

R is available as Free Software under the terms of the Free Software Foundation’s GNU General Public License in source code form. It compiles and runs on a wide variety of UNIX platforms and similar systems (including FreeBSD and Linux), Windows and MacOS.

[Download R for Linux](https://cran.r-project.org/bin/linux/)

[Download R for macOS](https://cran.r-project.org/bin/macosx/)

[Download R for Windows](https://cran.r-project.org/bin/windows/)

![RStudio Downloads](images/RStudioDownloads.png)

### Design of the R system

The primary R system is available from the Comprehensive R Archive Network, also known as [CRAN](https://cran.r-project.org/). CRAN also hosts many add-on packages that can be used to extend the functionality of R.

The R system is divided into 2 conceptual parts:

1. The “base” R system that you download from CRAN
2. Everything else.

R functionality is divided into a number of packages.

- The “base” R system contains, among other things, the base package which is required to run R and contains the most fundamental functions.

- The other packages contained in the “base” system include `utils`, `stats`, `datasets`, `graphics`, `grDevices`, `grid`, `methods`, `tools`, `parallel`, `compiler`, `splines`, `tcltk`, `stats4`.

- There are also “Recommended” packages: `boot`, `class`, `cluster`, `codetools`, `foreign`, `KernSmooth`, `lattice`, `mgcv`, `nlme`, `rpart`, `survival`, `MASS`, `spatial`, `nnet`, `Matrix`.

When you download a fresh installation of R from CRAN, you get all of the above, which represents a substantial amount of functionality. However, there are many other packages available:

- There are over 4000 packages on CRAN that have been developed by users and programmers around the world.

- There are also many packages associated with the [Bioconductor project](https://bioconductor.org/).

![R Bioconductor](images/Bioconductor.png)

- There are a number of packages being developed on repositories like GitHub and BitBucket but there is no reliable listing of all these packages.

Like for all programming languages there exists a code style guide to more easily understand and read R code.

[http://adv-r.had.co.nz/Style.html](http://adv-r.had.co.nz/Style.html)

[Next >>> What is RStudio](RSTUDIO.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
