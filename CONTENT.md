[Introduction <<<](https://mpievolbio-it.pages.gwdg.de/rforbeginners/)

## Content

The steps that this tutorial covers are:

[Introduction](https://mpievolbio-it.pages.gwdg.de/rforbeginners/)

[Content](CONTENT.html)

1. [What is R](R.html)
2. [What is RStudio](RSTUDIO.html)
3. [What is Rmarkdown](RMARKDOWN.html)
4. [R basics](RBASICS.html)
5. [R basic plotting](RBASICPLOTTING.html)
6. [Control structures](CONTROLSTRUCTURES.html)
7. [Loop Functions](LOOPFUNCTIONS.html)
8. [Regular expressions and strings](REGEXP.html)
9. [tidyverse](TIDYVERSE.html)
10. [ggplot2](GGPLOT2.html)
11. [Solutions](SOLUTIONS.html)

[Links](LINKS.html)

[References](SOURCES.html)

[Next >>> What is R](R.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
