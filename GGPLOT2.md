[tidyverse <<<](TIDYVERSE.html)

## ggplot2

As `ggplot2` is now commonly the standard way of plotting in R, there are a lot of good tutorials explaining the single ggplot2 functions and use-cases.

One very good start point is in the R book [R for data science](https://r4ds.had.co.nz/index.html):

[https://r4ds.had.co.nz/data-visualisation.html](https://r4ds.had.co.nz/data-visualisation.html)

In addition there exists a cheatsheet for `ggplot2` shwoing the basic functionality.

![ggplot2_cheat_sheet1](images/ggplot2_cheat_sheet1.png)

![ggplot2_cheat_sheet2](images/ggplot2_cheat_sheet2.png)

This section will shortly describe some basic functions.

{% hint style='info' %}
To become an expert in `ggplot2` it is recommended to look into the given existing tutorial resources and dedicated books.

[https://r4ds.had.co.nz/data-visualisation.html](https://r4ds.had.co.nz/data-visualisation.html)

[https://biocorecrg.github.io/CRG_RIntroduction_2021/ggplot2-package.html](https://biocorecrg.github.io/CRG_RIntroduction_2021/ggplot2-package.html)

[Hadley Wickham, UseR!, 2016, ggplot2 - Elegant Graphics for Data Analysis](https://link.springer.com/book/10.1007/978-3-319-24277-4)
{% endhint %}

### Grammar of graphics

`ggplot2` uses the basic units of the "grammar of graphics" to construct data visualizations in a layered approach.

The basic units in the "grammar of graphics" consist of:

- The **data** or the actual information that is to be visualized.
- The **geometries**, shortened to **geoms**, which describe the shapes that represent the data.
- These shapes can be dots on a scatter plot, bar charts on the graph, or a line to plot the data.
- Data are mapped to geoms.
- The **aesthetics**, or the visual attributes of the plot, including the scales on the axes, the color, the fill, and other attributes concerning appearance.

Visualizations in `ggplot2` begin with a blank canvas, which is just an empty plot with data associated to it.

Geoms are "added" as **layers** to the original canvas, adding representations of the data to the visualization.

In the example:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - mtcars
data(mtcars)
## Make plot
ggplot(data=mtcars, aes(x=wt, y=mpg)) +
    geom_point() +
    geom_smooth()
```

- The first line declares the data that will be used in the plot (mtcars) and creates the aesthetic mapping of wt to mpg.
- The second line creates the data point geom layer.
- The third line creates the smoothed geom layer.

The key is that the `aes()` aesthetic function on line one maps the data onto each of the two geom layers.

### Aesthetics

In `ggplot2` geom aesthetics are **data-driven** instructions that determine the visual properties of an individual geom.

Geom aesthetics defined in the `aes()` function allow individual layers of a visualization to have their own aesthetic mappings.

These aesthetic mappings can vary depending on the geom.

Ther is a rich description of all possible option in `ggplot2`:

[https://ggplot2.tidyverse.org/articles/ggplot2-specs.html](https://ggplot2.tidyverse.org/articles/ggplot2-specs.html).

For example, the `geom_point()` function geom can color-code the data points on a scatterplot based on a property with the following code:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - airquality
data(airquality)
## Make plot
viz <- ggplot(data=airquality, aes(x=Ozone, y=Temp)) +
    geom_point(aes(color=Month)) + 
    geom_smooth()
viz
```

Examples of ggplot2 aesthetics include:

- scales for the x and y axes
- color of the data points on the plot based on a property or on a color preference
- the size or shape of different geometries

Aesthetics are set either manually or by aesthetic mappings.

Aesthetic mappings **map** variables from the bound data frame to visual properties in the plot.

These mappings are provided in two ways using the `aes()` mapping function:

1. At the canvas level: All subsequent layers on the canvas will inherit the aesthetic mappings defined when the ggplot object was created with `ggplot()`.
2. At the geom level: Only that layer will use the aesthetic mappings provided.

Example aesthetics at canvas level:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - airquality
data(airquality)
## Make plot
viz <- ggplot(data=airquality, aes(x=Ozone, y=Temp)) +
    geom_point(aes(color=Month)) + 
    geom_smooth()
viz
```

Example aesthetics at geom level:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - airquality
data(airquality)
## Make plot
viz <- ggplot(data=airquality) +
  geom_point(aes(x=Ozone, y=Temp)) +
  geom_smooth(aes(x=Ozone, y=Temp))
viz
```

### Scatter plot

As for the basic plotting, a **scatter plot** has points that show the relationship between two sets of data.

In `ggplo2` the `geom_point()` function creates such a plot:

```r
## Create 2 vectors x and y with random numbers
x <- rnorm(100, 0, 1)
y <- rnorm(100, 0, 1)
## Create a data fram
df <- data.frame(x=x, y=y)
## Plot x against y
ggplot(data=df, mapping=aes(x=x, y=y)) +
    geom_point()
```

Add layers to that object to customize the plot:

- `ggtitle()` function to add a title
- `geom_vline()` function to add a vertical line

```r
ggplot(data=df, mapping=aes(x=x, y=y)) +
    geom_point() +
    ggtitle(label="Title") +
    geom_vline(xintercept=0)
```

{% hint style='tip' %}
With `ggplo2` one can store the plot easily to an object and add specific **geom** functions later on:

```r
## Create 2 vectors x and y with random numbers
x <- rnorm(100, 0, 1)
y <- rnorm(100, 0, 1)
## Create a data fram
df <- data.frame(x=x, y=y)
## Plot x against y and save in object p
p <- ggplot(data=df, mapping=aes(x=x, y=y)) +
    geom_point()
## Plot the object
p
## Re-use object p to add title and vline
p + ggtitle(label="Title") + geom_vline(xintercept=0)
```
{% endhint %}

The color of the points can be either set within the `geom_point()` function to one color:

```r
## set color of all points to red
p + ggtitle(label="Title") + geom_vline(xintercept=0) + geom_point(color="red")
```

Or if a grouping column exists can be colored by this columns:

```r
## Add arbitray group to the data frame df
df[["group"]] <- as.factor(sample(c("a", "b", "c"), 100, replace = TRUE))
## color the point by group column
ggplot(data=df, mapping=aes(x=x, y=y, color=group)) +
    geom_point()
```

### Box plot

To create a simple box plot for one column the `geom_boxplot()` function is used:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - iris
data(iris)
## Make box plot
ggplot(data=iris, aes(x=Sepal.Length)) +
    geom_boxplot() +
    coord_flip()
```

Split the data into 2 boxes, depending on the grouping column:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - iris
data(iris)
## Make box plot
ggplot(data=iris, mapping=aes(x=Species, y=Sepal.Length)) +
    geom_boxplot()
```

Color by grouping:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - iris
data(iris)
## Make box plot
ggplot(data=iris, mapping=aes(x=Species, y=Sepal.Length, color=Species)) +
    geom_boxplot()
```

Fill by grouping:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - iris
data(iris)
## Make box plot
ggplot(data=iris, mapping=aes(x=Species, y=Sepal.Length, fill=Species)) +
    geom_boxplot()
```

### Bar plot

The `geom_bar()` layer adds a bar chart to a `ggplot2` canvas.

Typically when creating a bar chart, an `aes()` aesthetic mapping with a single categorical value on the x axes and the `aes()` function will compute the count for each category and display the count values on the y axis.

To create a bar chart displaying the number of books in each Language from a books data frame:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - airquality
data(airquality)
## Make plot
bar <- ggplot(airquality, aes(x=Wind)) + geom_bar()
bar
```

To flip the bar plot use the `coord_flip()` function:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - airquality
data(airquality)
## Make plot
bar <- ggplot(airquality, aes(x=Wind)) + geom_bar()
bar
bar.flipped <- bar + coord_flip()
bar.flipped
```

#### Bar plot with error bars

We can create error bars on barplots.

{% hint style='info' %}
If you supply values to the `geom_bar()` function that should not be "counted" to create the bar plot, you need to set the `stat` parameter to `stat = "identity"` and not to the default `stat = "count"`.
{% endhint %}

A toy data set, that contains 7 independent measurements is used:

```r
## Create toy data set
bar.values <- data.frame(
    Dkk1=c(18.2, 18.1, 17.8, 17.85, 18.6, 12.4, 10.7),
    Pten=c(15.1,15.2, 15.0, 15.6, 15.3, 14.8, 15.9),
    Tp53=c(9.1, 9.9, 9.25, 8.7, 8.8, 9.3, 7.8))
bar.values
```

The height of the bar will represent the average measurement.

The error bar will represent the average minus standard deviation on the lower part, and the average plus standard deviation on the high part.

To create a data frame that summarizes these values we will use the `apply()` function which can apply a function to either columns or rows of a **matrix** or **data frame**:

```r
## Use the apply() function to calculate the average and standard deviation
## Addtional the names will be added as a group
bar.summary <- data.frame(average=apply(bar.values, 2, mean),
    standard_deviation=apply(bar.values, 2, sd),
    genes=colnames(bar.values))
bar.summary
```

The `geom_errorbar()` function will be used to add this layer to the bar plot.

The average minus standard deviation and average plus standard deviation will be calculated on-the-fly:

```r
ggplot(bar.summary, aes(x=genes, y=average, fill=genes)) + 
    geom_bar(stat = "identity") +
    geom_errorbar(aes(ymin=average-standard_deviation,
        ymax=average+standard_deviation), colour="black", width=.1)
```

### Histogram

Simple histogram on one sample (using the iris data frame) can be obtained via the `geom_histogram()` function:

```r
## Load ggplot2 package
library(ggplot2)
## Load example data - iris
data(iris)
## Make histogram of value Sepal.Length
ggplot(data=iris, mapping=aes(x=Sepal.Length)) +
    geom_histogram()
```

Split the data per sample ("Species" column that represents here the species):

```r
ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram()
```

By default, the histograms are **stacked**.

This can be changed to side by side by setting the parameter `position=dodge`:

```r
ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram(position='dodge')
```

### Labels

In `ggplot2`, **labels** add meaning and clarity to a data visualization.

`ggplot2` automatically assigns the name of the variable corresponding to components, like axes labels. Because data frame variable names are not always legible to outside readers, the `labs()` function allows you to manually set labels.

To customize a plot's labels, add a `labs()` function call to the ggplot object. Inside the function call to `labs()`, you can provide labels for the x and y axes as well as a `title`, `subtitle`, or `caption`. The list of available label arguments can be found in the [`labs()` documentation](https://ggplot2.tidyverse.org/reference/labs.html).

```r
## Load example data - airquality
data(airquality)
## Make plot
viz <- ggplot(airquality, aes(x=Ozone, y=Temp)) + 
    geom_point() +
    labs(title="New York Air Quality Measurements",
        subtitle="Daily air quality measurements in New York, May to September 1973.",
        x="Ozone (ppb)",
        y="Temperature (degrees F)")
viz
```

### split plot by category

A plot can be splitted with the `facet_wrap()` function based on a column.

The "New York Air Quality Measurements" data `data(airquality)` is a data frame with 153 observations on 6 variables.

1. Ozone - numeric Ozone (ppb)
2. Solar.R - numeric Solar R (lang)
3. Wind - numeric Wind (mph)
4. Temp - numeric Temperature (degrees F)
5. Month - numeric Month (1--12)
6. Day - numeric Day of month (1--31)

One can use a category and split the plot accordingly, e.g. by Month:

```r
## Load example data - airquality
data(airquality)
## Make plot
viz <- ggplot(airquality, aes(x=Day, y=Temp)) + 
    geom_point() +
    geom_smooth() +
    labs(title="New York Air Quality Measurements",
        subtitle="Daily air quality measurements in New York, May to September 1973.",
        x="Day of month (1--31)",
        y="Temperature (degrees F)") +
    facet_wrap(~Month)
viz
```

### save plots

One could either use the default save plot functions as before `png()`, `jpg()`, `pdf()`:

```r
## Load ggplot2 package
library(ggplot2)

png("iris.Sepal.Length.side_by_side.histogram.png")
ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram(position='dodge')
dev.off()
```

Or one can use the ``ggsave()` function from ggplot2:

```r
ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram(position='dodge')
## By default, save the last plot that was produced
ggsave(filename="lastplot.png")


## You can pick which plot you want to save:
p1 <- ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram()
p2 <- ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram(position='dodge')
ggsave(filename="iris.Sepal.Length.histogram.png",
    plot=p1)
ggsave(filename="iris.Sepal.Length.side_by_side.histogram.png",
    plot=p2)

## Many different formats are available: 
## "eps", "ps", "tex", "pdf", "jpeg", "tiff", "png", "bmp", "svg", "wmf"
ggsave(filename="iris.Sepal.Length.histogram.pdf",
    plot=p1,
    device="pdf")

## Change the height and width (and their unit):
ggsave(filename="iris.Sepal.Length.side_by_side.histogram.pdf",
    plot=p1,
    device="pdf",
    width = 20, 
    height = 20, 
    units = "cm")
```

### Several plots on one page

You can also organize several plots on one page using the `gridExtra` package.

```r
## Install the gridExtra package
install.packages("gridExtra")
## Load ggplot2 package
library(ggplot2)
## Create some plots
p1 <- ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram()
p2 <- ggplot(data=iris, mapping=aes(x=Sepal.Length, fill=Species)) +
    geom_histogram(position='dodge')
## Arrange the plots with grid.arrange()
grid.arrange(p1, p2)
```

### Exercises

[Next >>> Solutions](SOLUTIONS.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
