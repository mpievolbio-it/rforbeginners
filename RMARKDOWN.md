[What is RStudio <<<](RSTUDIO.html)

## Rmarkdown

Markdown is a lightweight markup language for creating formatted text using a plain-text editor.

**Rmarkdown** is the port of it which makes you:

- describe your research in a fast and reproducible way
- create reports for sharing analysis methods, software code and results

**Rmarkdown** is a file format in its most basic form, that can eventually be converted into a shareable document, e.g HTML, PDF and many others.

It allows you to document not just your R (Python and SQL) code, but also enables the inclusion of tables, figures, along with descriptive text.

**Thus resulting in a final document that has the methods, the code and interpretation of results all in a single document!**

![rmarkdown_workflow](images/rmarkdown_workflow.png)

![RMarkdownKnitrOutputs](images/RMarkdownKnitrOutputs.png)

### Rmarkdown basics

{% hint style='info' %}
The RStudio cheatsheet for Rmarkdown is quite daunting,
but includes more advanced Rmarkdown options that may be helpful as you become familiar with report generation,
including options for adding interactive plots RShiny.
{% endhint %}

![rmarkdown1](images/rmarkdown1.png)

![rmarkdown2](images/rmarkdown2.png)

A **Rmarkdown** file consists of three parts:

![RMarkdownFileOverview](images/RMarkdownFileOverview.png)

1. YAML metadata

```
---
title: "Title"
author: "Author"
date: "`r Sys.Date()`"
output: html_document
---
```

This section has information listed in YAML format, and is usually used to specify metadata (title, author)
and basic configuration information (output format) associated with the file.
You can find detailed information about specifications that can be made in this section on
[this webpage](https://bookdown.org/yihui/rmarkdown/html-document.html).

2. text

```
# Header
This is an example text.

## Sub-Header

### Sub-Sub-Header
```

The syntax for formatting the text portion of the report is relatively easy.
You can easily get text that is **bolded**, *italicized*, ***bolded and italicized***.
You can create "headers" and "sub-headers" to organize the information by placing an "#" or "##" and so on in front of a line of text,
generate numbered and bulleted lists, add hyperlinks to words or phrases, and so on.

The syntax follow the basic Markdown syntax.

See here to get to know the basic Markdown syntax [https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax).

3. code chunks

![RStudioCodeChunk](images/RStudioCodeChunk.png)

The basic idea behind Rmarkdown is that you can describe your analysis workflow and provide interpretation of results in plain text,
and intersperse chunks of R code within that document to tell a complete story using a single document.

Code chunks in RMarkdown are delimited with a special marker (\`\`\`). Backticks (`) commonly indicate a chunk of code.

Each individual code chunk should be given a unique name.

The name should be something meaningful, and we recommend to **not use whitespace** but **underscores**.

There is a handy Insert button within RStudio that allows you to insert an empty R chunk in your document without having to type the backticks etc. yourself.

{% hint style='tip' %}
RStudio keyboard shortcuts to insert a R code chunk:

- hit "Ctrl+Alt+i" in Windows
- hit "command+option+i" ("⌘+⌥+i") on OS X
{% endhint %}

![RStudioInsertCodeChunk](images/RStudioInsertCodeChunk.png)

{% hint style='tip' %}
Each code chunk can be run in RStudio using the "Run Current Chunk" button.
{% endhint %}

> #### Note::Exercise 001
> Use the **Knit** button to **knit the markdown** file into a HTML

![RStudioKnit](images/RStudioKnit.png)

In the console, you will see a flurry of text scrolling by.
The text indicates progress while each code chunk is being executed.
Once the document is finished **knitting**, a new window will be automatically opening up with the HTML report that was just generated.

See here to get to know the basic Markdown syntax [https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax).

See here how to render mathematical notations [https://rpruim.github.io/s341/S19/from-class/MathinRmd.html](https://rpruim.github.io/s341/S19/from-class/MathinRmd.html).

> #### Note::Exercise 002
> text: create at least one header, one sub-header, one ordered list, one url link, one mathematical notation
> 
> code chunk: create at least two code blocks

[Next >>> R basics](RBASICS.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
