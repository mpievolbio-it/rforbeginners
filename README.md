## R for Beginners

The source code of this tutorial can be obtained here:

[https://gitlab.gwdg.de/mpievolbio-it/rforbeginners](https://gitlab.gwdg.de/mpievolbio-it/rforbeginners)

The webpage of this book is hosted here:

[https://mpievolbio-it.pages.gwdg.de/rforbeginners/](https://mpievolbio-it.pages.gwdg.de/rforbeginners/)

This tutorial is a basic R for Beginners tutorial, which combines different R for Beginners Courses (see list of sources).

It covers the following topics:

- R
- RStudio environment
- Rmarkdown
- R basics
  - Help
  - Functions
  - Packages
  - Data types
  - Data structures
  - Data assignment, access, subset
  - Missing values
  - Data import/export
- R basic plotting
  - plot()
  - barplot()
  - boxplot()
  - hist()
  - pie()
  - image()
  - heatmap()
- Control structures
  - if() and else()
  - for() loops
  - nested for() loops
  - while() loops
- Loop Functions on list(), matrix() and data.frame()
  - lapply()
  - apply()
  - colSums(), rowSums()
- Regular expressions and strings
  - grep()
  - sub() and gsub()
- tidyverse
  - readr
  - stringr
  - dplyr
  - dplyr pipe %>%
  - tibble
  - tidyr
- ggplot2

## Introduction

R is commonly used in many scientific disciplines for statistical analysis and bioinformatics. It benefits from a bunch of additional packages, which can be obtained via [https://cran.r-project.org/web/packages/available_packages_by_name.html](https://cran.r-project.org/web/packages/available_packages_by_name.html) or [https://bioconductor.org/](https://bioconductor.org/).

The goal of this "R for Beginners" tutorial is to help you learn the most important tools in R that will allow you to do data science.

[Next >>> Content](CONTENT.html)

----

These notes have been prepared by Kristian K Ullrich. The copyright for the material in these notes resides with the author.

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
