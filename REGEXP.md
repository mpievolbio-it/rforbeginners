[Loop Functions on list(), matrix() and data.frame() <<<](LOOPFUNCTIONS.html)

## Regular expressions and strings

Regular expressions are tools to **describe patterns in strings**.

![regex_cheat_sheet](images/regex_cheat_sheet.png)

### Find simple matches with grep

In R the `grep()` function can be used to search for patterns and either retrun the **index** or the **value** itself.

```r
?grep
```

Find a **pattern** anywhere in the string (outputs the index of the element):

```r
## By default, outputs the index of the element matching the pattern
grep(pattern="Gen", 
    x=c("Genomics", "genomics", "GEN"))
```

Show actual element where the pattern is found (instead of the index only) with the parameter `value=TRUE`:

```r
## Set value=TRUE
grep(pattern="Gen",
    x=c("Genomics", "genomics", "GEN"),
    value=TRUE)
```

A **non case-sensitive** search can be activated with the parameter `ignore.case=TRUE`:

```r
## Enter the pattern in lower-case, but case is ignored
grep(pattern="Gen",
    x=c("Genomics", "genomics", "GEN"),
    ignore.case=TRUE)
```

Show if it **DOESN’T** match the pattern with the parameter `inv=TRUE`:

```r
## Shows what doesn't match
grep(pattern="Gen",
    x=c("Genomics", "genomics", "GEN"),
    inv=TRUE)
```

### Regular expressions to find more flexible patterns

Special characters used for pattern recognition:

| Character | Description |
|:---------:|:-----------:|
| $ | Find pattern at the end of the string |
| ^ | Find pattern at the beginning of the string |
| {n} | The previous pattern should be found exactly n times |
| {n,m} | The previous pattern should be found between n and m times |
| + | The previous pattern should be found at least 1 time |
| * | One or more allowed, but optional |
| ? | One allowed, but optional |

Match your own pattern inside []:

| Pattern | Description |
|:-------:|:-----------:|
| [abc] | matches a, b, or c |
| 1 | matches a, b or c at the beginning of the element |
| ^A[abc]+ | matches A as the first character of the element, then either a, b or c |
| ^A[abc]* | matches A as the first character of the element, then optionally either a, b or c |
| ^A[abc]{1}_ | matches A as the first character of the element, then either a, b or c (one time!) followed by an underscore |
| [a-z] | matches every character between a and z |
| [A-Z] | matches every character between A and Z |
| [0-9] | matches every number between 0 and 9 |

Predefined variables to use in regular expressions:

| Variable | Description |
|:--------:|:-----------:|
| [:lower:] | Lower-case letters |
| [:upper:] | Upper-case letters |
| [:alpha:] | Alphabetic characters: [:lower:] and [:upper:] |
| [:digit:] | Digits: 0 1 2 3 4 5 6 7 8 9 |
| [:alnum:] | Alphanumeric characters: [:alpha:] and [:digit:] |
| [:print:] | Printable characters: [:alnum:], [:punct:] and space. |
| [:punct:] | Punctuation characters: ! " # $ % & ’ ( ) * + , - . / : ; < = > ? @ [  ] ^ _ ` { \| } ~ |
| [:blank:] | Blank characters: space and tab |

Match anything contained between brackets (here either g or t) once:

```r
grep(pattern="[gt]", 
    x=c("genomics", "proteomics", "transcriptomics"), 
    value=TRUE)
```

Match anything contained between brackets once AND at the start of the element:

```r
grep(pattern="^[gt]",
    x=c("genomics", "proteomics", "transcriptomics"),
    value=TRUE)
```

### Substitute or remove matching patterns with gsub

{% hint style='info' %}
The `sub()` function perform replacement of the first occurence, wherease the `gsub()` function replases all matches.
{% endhint %}

```r
gsub(pattern="Gen",
    replacement="GENE",
    x=c("Genomics", "genomics", "GEN"))
```

Again the parameter `ignore.case=TRUE` set the search to **non case-sensitive**:

```r
gsub(pattern="Gen",
    replacement="GENE",
    x=c("Genomics", "genomics", "GEN"),
    ignore.case=TRUE)
```

### Retrieve columns by their names

```r
m <- matrix(rnorm(100, 0, 1), ncol=10, nrow=10)
colnames(m) <- letters[1:10]
m
m[, grep("[abdg]", colnames(m))]
```

### Get a sub string

To get a sub string from a **character** vector one can use the `substring()` function:

```r
a <- "This is a sentence. And another one."
substr(x=a, start=5, stop=10)
```

### Split a string into a list by pattern

A **character** vector can be split with the `strsplit()` function which will return a list of splits.

```r
a <- "This is a sentence. And another one."
mySplit <- strsplit(x=a, split=" ")
mySplit
str(mySplit)
```

### Exercises

[Next >>> tidyverse](TIDYVERSE.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
