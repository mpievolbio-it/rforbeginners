[Control structures <<<](CONTROLSTRUCTURES.html)

## Loop Functions on list(), matrix() and data.frame()

To obtain e.g. `mean()` values for all items of a **list**, all columns from a **matrix** or a **data frame** one can use the `mean()` function on each column individually, but there is also an easier way to go about it.

Programming languages typically have a way to allow the execution of a single line of code or several lines of code multiple times, or in a **loop**.

By default R is not very good at looping, hence the `apply()` family of functions are used for this purpose.

This family includes several functions, each differing slightly on the inputs or outputs.

| Function | Description |
|:--------:|:-----------:|
| apply | Apply Functions Over Array Margins |
| by | Apply a Function to a Data Frame Split by Factors |
| eapply | Apply a Function Over Values in an Environment |
| lapply | Apply a Function over a List or Vector (returns list) |
| sapply | Apply a Function over a List or Vector (returns vector) |
| mapply | Apply a Function to Multiple List or Vector Arguments |
| rapply | Recursively Apply a Function to a List |
| tapply | Apply a Function Over a Ragged Array |

For example, we can use the `lapply()` function for **lists** and the `apply()` function either on columns or rows from a **matrix** or a **data frame**.

### lapply

The `lapply()` function takes 2 parameter:

- `x` the **list**
- `FUN` the function to be applied on each list item

```r
x <-  1:10
y <- 11:20
z <- 21:30
myList <- list(x=x, y=y, z=z)
myList
lapply(X=myList, FUN=mean)
```

One might need to use additional parameter for the `FUN` function.

```r
x <-  1:10
y <- 11:20
y[4] <- NA
z <- 21:30
myList <- list(x=x, y=y, z=z)
myList
lapply(X=myList, FUN=mean)
lapply(X=myList, FUN=function(x){mean(x, na.rm=TRUE)})
```

### apply

The `apply()` function is used to a evaluate a function (often an anonymous one) over the margins of an array.

It is most often used to apply a function to the rows or columns of a **matrix** (which is just a 2-dimensional array).

The arguments to `apply()` are

- `X` is an array
- `MARGIN` is an integer vector indicating which margins should be "retained".
- `FUN` is a function to be applied
- `...` is for other arguments to be passed to FUN

```r
x <- 1:10
myMatrix <- matrix(x, ncol=10, nrow=10)
myMatrix
## apply the sum() function to each row
apply(myMatrix, 1, sum)
## apply the sum() function to each column
apply(myMatrix, 2, sum)
```

### colSums rowSums colMeans rowMeans

For the special case of column and row sums and column and row means of matrices, there are some useful shortcuts.

The `rowSums()` function equals `apply(x, 1, sum)`.

The `rowMeans()` function equals `apply(x, 1, mean)`.

The `colSums()` function equals `apply(x, 2, sum)`.

The `colMeans()` function equals `apply(x, 2, mean)`.

> #### Note::Exercise 021
> Write your own `colSD()` and `rowSD()` function.
> ```r
> colSD <- function(){
> 
>     return()
> }
>
> rowSD <- function(){
> 
>     return()
> }
> ```

### mapply

The `mapply()` function is a multivariate apply of sorts which applies a function in parallel over a set of arguments.

The arguments to `mapply()` are

- `FUN` is a function to apply
- `...` contains R objects to apply over
- `MoreArgs` is a list of other arguments to FUN.
- `SIMPLIFY` indicates whether the result should be simplified

For example, the following is tedious to type:

```r
list(rep(1, 4), rep(2, 3), rep(3, 2), rep(4, 1))
```

With `mapply()`, instead we can do:

```r
mapply(rep, 1:4, 4:1)
```

This passes the sequence `1:4` to the first argument of `rep()` and the sequence `4:1` to the second argument.

[Next >>> Regular expression](REGEXP.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
