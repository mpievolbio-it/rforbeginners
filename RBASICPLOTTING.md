[R basics <<<](RBASICS.html)

## R basic plotting

This part describes the basic plotting functions in R.

Anyhow, there are dedicated R packages that deal with static `ggplot2` and interactive `plotly` graphs in much more detail.

[ggplot2](https://ggplot2.tidyverse.org/)

[plotly](https://plotly.com/r/)

### plot margins

R plotting margins can be set with the `par()` function.

```r
?par
```

![plot margins](images/PlotMargins.png)

### plot colors

Color codes 1 to 8 are taken from the `palette()` function and respectively code for:

```r
## see the 8-color palette:
palette()
```

There is a larger set of build-in colors that you can use:

```r
## see all 657 possible build-in colors:
colors()

## looking for blue only? You can pick from 66 blueish options:
grep(pattern="blue", x=colors(), value=TRUE)
```

There are dedicated R packages that deal with colors and color palettes.

[viridis](https://cran.r-project.org/web/packages/viridis/vignettes/intro-to-viridis.html)

[RColorBrewer](https://cran.r-project.org/web/packages/RColorBrewer/index.html)

[https://github.com/zumbov2/colorfindr](https://github.com/zumbov2/colorfindr)

[https://github.com/loukesio/ltc_palettes](https://github.com/loukesio/ltc_palettes)

[https://github.com/karthik/wesanderson](https://github.com/karthik/wesanderson)

And even a package with Scientific Journal related color schemes.

[ggsci](https://cran.r-project.org/web/packages/ggsci/vignettes/ggsci.html)

### plot

The default `plot()` function will create a **scatter plot**.

Anyhow, the **generic plot function** might behave differently according to the individual package implementation based on the object class.

A **scatter plot** has points that show the relationship between two sets of data.

```r
?plot
```

```r
## Create 2 vectors
dat1 <- 1:10
dat2 <- dat1^2
## Plot x against y
plot(x=dat1, y=dat2)
```

x and y can also be the columns of a matrix or a data frame.

```r
## Create 2 vectors
dat1 <- 1:10
dat2 <- dat1^2
## Combine into a matrix
m <- cbind(dat1, dat2)
## Plot first column from matrix m against second column
plot(x=m[,1], y=m[,2])
```

One can add basic arguments to change the plot:

- col: color
- pch: type of point
- type: "l" for line, "p" for point, "b" for both point and line
- main: title of the plot
- cex: size of points (default: 1)

```r
## Create 2 vectors
dat1 <- 1:10
dat2 <- dat1^2
## Plot x against y defining some plotting options
plot(x=dat1, y=dat2, 
    col="red", 
    pch=2, 
    type="b", 
    main="a pretty scatter plot")
```

> #### Note::Exercise 017
> The datasets package (included in the base installation of R) contains pre-made / built-in datasets.
> 
> You can see them in Environment >>> change Global Environment to package:datasets.
> 
> Here, you will use the data set Loblolly (growth of Loblolly pine trees):
> ```r
> ## load Loblolly data
> data("Loblolly")
> dim(Loblolly)
> head(Loblolly)
> ```
> 
> - Plot age (x-axis) versus height (y-axis).
> - Change the title.
> - Change the points type to a full triangle (see plot pch parameter).
> - Change the points color to the color of your choice.
> - Change the points size to 0.4.

### barplot

A bar chart or bar plot displays rectangular bars with lengths proportional to the values that they represent.

```r
?barplot
```

A simple barplot:

```r
## Create a vector
mycenter <- rep(x=c("PhDstudent", "Postdoc", "Technician", "PI"), 
    times=c(8,10,5,2))
## Count number of occurrences of each character string
mytable <- table(mycenter)
## Bar plot using that table
barplot(height=mytable)
```

Customize a bit :

- col : color
- main : title of the plot
- las : orientation of axis labels:
    0: all labels parallel to axis
    1: x-axis labels parallel / y-axis labels perpendicular
    2: both labels perpendicular
    3: x-axis labels perpendicular / y-axis labels parallel

```r
## Create a vector
mycenter <- rep(x=c("PhDstudent", "Postdoc", "Technician", "PI"), 
    times=c(8,10,5,2))
## Count number of occurrences of each character string
mytable <- table(mycenter)
## Bar plot using that table
barplot(height=mytable,
    col=1:4,
    main="bar plot",
    las=2)
```

stacked barplot :

```r
## Create a matrix containing the number and type of employees per research program :
barmat <- matrix(c(8, 10, 9, 2, 6, 4, 5, 3, 14, 13, 16, 4, 11, 10, 8, 5),
    nrow=4,
    dimnames=list(c("Technician", "PhDstudent", "PostDoc", "PI"), c("BG", "CDB", "GRSCC", "SB")))
## Plot barplot
barplot(height=barmat, col=sample(colors(), 4))
```

Add a legend to the plot:

- "x" and "y" set the legend’s position in the plotting area: you can specify the position as coordinates using "x" and "y".
- if "x" only is used, you can set the legend position as "topleft", "bottomleft", "topright", "bottomright"
- Note: `barplot()` (or any other plot function) has to be called first

```r
## Create a matrix containing the number and type of employees per research program :
barmat <- matrix(c(8, 10, 9, 2, 6, 4, 5, 3, 14, 13, 16, 4, 11, 10, 8, 5),
    nrow=4,
    dimnames=list(c("Technician", "PhDstudent", "PostDoc", "PI"), c("BG", "CDB", "GRSCC", "SB")))
## set a random color vector
mycolors <- sample(x=colors(), 
    size=4)
## Plot barplot
barplot(height=barmat, 
    col=mycolors, 
    ylim=c(0,50),
    main="stacked barplot")
## Add legend
legend(x="topleft", 
    legend=c("Technician", "PhDstudent", "PostDoc", "PI"),
    fill=mycolors)
```

### boxplot

A boxplot is a convenient way to describe the **distribution** of the data.

```r
?boxplot
```

A simple boxplot:

```r
## Create a matrix of 1000 random values from the normal distribution (4 columns, 250 rows)
mat1000 <- matrix(rnorm(1000), 
    ncol=4)
## Basic boxplot
boxplot(x=mat1000)
```

Add some arguments :

- xlab: x-axis label
- ylab: y-axis label

```r
## Create a matrix of 1000 random values from the normal distribution (4 columns, 250 rows)
mat1000 <- matrix(rnorm(1000), 
    ncol=4)
## Basic boxplot
boxplot(x=mat1000,
    xlab="sample",
    ylab="expression")
```

One can also create a boxplot that plots a variable against another variable.

For example, going back to the Loblolly data frame, one can create a boxplot of the height (y-axis) for each age (x-axis):

one box per age group.

Instead of setting parameter x we set parameter **formula**, as follows:

```r
data("Loblolly")
boxplot(formula=Loblolly$height ~ Loblolly$age)
```

### hist

A histogram is a graphical representation that organizes a group of data points into user-specified ranges.

```r
?hist
```

The `hist()` function by default will set the ranges automatically.

One can use a distribution function to create test data, e.g. the normal distribution `rnorm()` function.

A simple histogram:

```r
## create normal distributed test data; 100 values with a mean of 0 and sd of 1
x <- rnorm(100, mean=0, sd=1)
hist(x)
```

### pie

Pie charts are not recommended in the R documentation, and their features are somewhat limited. The authors recommend bar or dot plots over pie charts because people are able to judge length more accurately than volume.

```r
## Define some data
slices <- c(10, 12,4, 16, 8)
## Define the labels
lbls <- c("US", "UK", "Australia", "Germany", "France")
## Create the pie chart and main title
pie(slices, labels = lbls, main="Pie Chart of Countries")
```

### image

Creates a grid of colored or gray-scale rectangles with colors corresponding to the values.

{% hint style='info' %}
The data to be used with the `image()` function needs to be a **numeric** matrix.

One can use the `as.matrix()` function to convert a **data frame** object.
{% endhint %}

```r
?image
```

```r
## Load example data - iris
data(iris)
## Image the first 4 columns of the iris data set
image(as.matrix(iris[,1:4]))
```

To change columns and rows, here we transpose the matrix with the `t()` function.

```r
## Load example data - iris
data(iris)
## Image the first 4 columns of the iris data set after transposition
image(t(as.matrix(iris[,1:4])))
```

### heatmap

A heatmap is a graphical representation of data where the values are represented with colors.

The values are by default clustered with the `hclust()` function and shown next to the columns and rows.

{% hint style='info' %}
The data to be used with the `heatmap()` function needs to be a **numeric** matrix.

One can use the `as.matrix()` function to convert a **data frame** object.
{% endhint %}

```r
?heatmap
```

```r
## Load example data - iris
data(iris)
## Heatmap of the first 4 columns of the iris data set
heatmap(as.matrix(iris[,1:4]))
```

### pheatmap

The `pheatmap()` function from the `pheatmap` package offers a better control over some graphical parameters than the basic `heatmap()` function.

{% hint style='tip' %}
The `pheatmap()` function of the `pheatmap` package can deal with **data frames**.
{% endhint %}

> #### Note::Exercise 018
> Install the `pheatmap` package and use the `pheatmap()` function to plot the first 4 columns of the `iris` data.
> 
> ```r
> ## install pheatmap package
> install.packages("pheatmap")
> ## Load pheatmap package
> 
> ## Load example data - iris
> 
> ## Create heatmap of the first 4 columns of the iris data set using the pheatmap() function
> 
> ```

### venn diagram

To create a venn diagram you will use the `ggvenn` package.

> #### Note::Exercise 019
> Install the `ggvenn` package.
> 
> ```r
> ## install ggvenn package
> install.packages("ggvenn")
> ```

To get the overlap the data needs to be in a named **list** object.

```r
## Create a named list object with random numbers of length 20
list_venn <- list(A = sort(sample(1:100, 20)),
    B = sort(sample(1:100, 20)),
    C = sort(sample(1:100, 20)),
    D = sort(sample(1:100, 20)))
```

The `ggvenn()` function takes the list as input to find the corresponding overlaps.

```r
## Load ggvenn package
library(ggvenn)
## Plot venn diagram
ggvenn(list_venn)
```

One can also just select a subset of the list to evaluate their individual overlaps based on the list names.

```r
ggvenn(list_venn, c("A", "C", "D")) 
```

### save plots

One important point is saving the plots. This can be aachieved either via RStudio by saving a plot into a file or via the **console**.

#### with RStudio

In RStduio one can use in the **Plot** window the tab **Export >>> Save as Image ...** or the **Export >>> Save as PDF ...** functionality.

![RStudioSavePlot](images/RStudioSavePlot.png)

#### with Console

Save a plot in the **console** is a three-step  process.

1. One opens a connection to an output file directly specifying the `width`, `height` and quality parameter.
2. One creates the plot with one of the functions explained to you (see above).
3. One closes the connection to the output file with the `dev.off()` function.

There are several functions for a broad range of file formats:

- `pdf()`
- `png()`
- `jpeg()`
- `bmp()`
- `tiff()`

{% hint style='info' %}
Please take a look ath the individual help for each file format function to see the possible quality parameter.
{% endhint %}

```r
?png
```

```r
?pdf
```

There is a dedicated R package that deals with plotting `ggplot2`.
We will have a dedicated look at it later in this tutorial to show the basic functions of `ggplot2`.

[https://ggplot2.tidyverse.org/](https://ggplot2.tidyverse.org/)

### Exercises

[Next >>> Control structures](CONTROLSTRUCTURES.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
